# REST Note App
Creates note for saving informations and any other data.

[ ![Codeship Status for zd-zilch/rest-note-app](https://app.codeship.com/projects/cc5685e0-ff3c-0135-aeeb-56b253369268/status?branch=master)](https://app.codeship.com/projects/279640)
[![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://bitbucket.org/zd-zilch/rest-note-app/issues/)
[![Open Source Love](https://badges.frapsoft.com/os/mit/mit.svg?v=102)](https://github.com/ellerbrock/open-source-badge/)
[![Semver](http://img.shields.io/SemVer/2.0.0.png)](http://semver.org/spec/v2.0.0.html)

## Getting Started
### Prerequisites
* Java JDK 1.8+
* Maven 3.2.5+
* Any IDE or Text Editor but IntelliJ is recommended
* Postman (optional: Just to check the endpoints)

### Installing
1. Clone the repository and checkout `master` branch
2. Open a terminal on the root folder and run `mvn install`

### Usage example
#### Run
  * By using packaged jar - `java -jar /target/REST-Note-App-x.x.x.jar`
  * By Maven using terminal - `mvn spring-boot:run`

#### Use the current endpoint: `localhost:8080/api/v1/notes` on Postman
#### Create a `GET` request on the endpoint.

## Running the Tests
* Open the run configurations and select the JUnit or
* Run on the test package

### Built With
* [Spring Boot](https://projects.spring.io/spring-boot/) - Java Framework Used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Free SQL Database](http://www.freesqldatabase.com/) - MySQL Host Database used
* [JUnit](http://junit.org/junit5/) - Tool used for testing
* [Thymeleaf](https://www.thymeleaf.org/) - Java Template Engine
* [SASS](https://sass-lang.com/) - CSS Preprocessor

### Versioning
[SemVer](https://semver.org/) is used for versioning.

### License
This project is licensed under the MIT License. See [LICENSE](/LICENSE) for more information.

### Contributing
1. Fork IT!
2. Create your own branch based from `dev` branch
3. Code
4. Create Pull Request!

#### Git Flow
This project uses [Git branching model](http://nvie.com/posts/a-successful-git-branching-model/)

### Acknowledgements
* Lars Norlander - My coach and instructor that reviews it and to improve my application.
