package zd.zilch.restnoteapp.RESTNoteApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class RestNoteAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestNoteAppApplication.class, args);
	}
}
