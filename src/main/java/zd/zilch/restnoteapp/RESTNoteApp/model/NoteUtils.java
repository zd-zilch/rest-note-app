package zd.zilch.restnoteapp.RESTNoteApp.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class NoteUtils {

    public static String prettifyDate(Date date) {
        return new SimpleDateFormat("EEE, MMM d yyyy", Locale.getDefault()).format(date);
    }
}
