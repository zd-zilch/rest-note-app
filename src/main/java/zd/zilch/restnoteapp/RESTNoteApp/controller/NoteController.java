package zd.zilch.restnoteapp.RESTNoteApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import zd.zilch.restnoteapp.RESTNoteApp.model.Note;
import zd.zilch.restnoteapp.RESTNoteApp.service.NoteService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/notes")
public class NoteController {

    @Autowired
    private NoteService noteService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Note> addNote(@RequestBody Note note) {
        return noteService.addNote(note);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Note> getAllNotes(@RequestParam(value = "q", required = false) String query) {
        return noteService.getAllNotes(query);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Note> getNoteById(@PathVariable Long id) {
        return noteService.getNoteById(id);
    }

    @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Note> updateNoteById(@PathVariable Long id, @RequestBody Note note) {
        return noteService.updateNoteById(id, note);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Note> deleteNoteById(@PathVariable Long id) {
        return noteService.deleteNoteById(id);
    }
}
