package zd.zilch.restnoteapp.RESTNoteApp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import zd.zilch.restnoteapp.RESTNoteApp.dao.NoteDao;
import zd.zilch.restnoteapp.RESTNoteApp.model.Note;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class NoteService {

    @Autowired
    private NoteDao noteDao;

    public List<Note> getAllNotes(String query) {
        List<Note> noteList = (List<Note>) noteDao.findAll();
        if (query != null) {
            noteList = noteList.stream()
                    .filter(n -> n.getContent().contains(query))
                    .collect(Collectors.toList());
        }

        return noteList;
    }

    public ResponseEntity<Note> addNote(Note note) {
        return ResponseEntity.ok(noteDao.save(note));
    }

    public ResponseEntity<Note> getNoteById(Long id) {
        Optional<Note> noteOptional = noteDao.findById(id);
        return noteOptional.map(note -> ResponseEntity.ok().body(note)).orElseGet(() -> ResponseEntity.notFound().build());
    }

    public ResponseEntity<Note> updateNoteById(Long id, Note newNote) {
        Optional<Note> noteOptional = noteDao.findById(id);
        if (!noteOptional.isPresent()) return ResponseEntity.notFound().build();

        Note note = noteOptional.get();
        note.setTitle(newNote.getTitle());
        note.setContent(newNote.getContent());
        return ResponseEntity.ok(noteDao.save(note));
    }

    public ResponseEntity<Note> deleteNoteById(Long id) {
        Optional<Note> note = noteDao.findById(id);
        if (!note.isPresent()) return ResponseEntity.notFound().build();

        noteDao.delete(note.get());
        return ResponseEntity.ok().build();
    }
}
