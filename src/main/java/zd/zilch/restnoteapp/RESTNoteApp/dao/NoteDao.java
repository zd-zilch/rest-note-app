package zd.zilch.restnoteapp.RESTNoteApp.dao;

import org.springframework.data.repository.CrudRepository;
import zd.zilch.restnoteapp.RESTNoteApp.model.Note;

public interface NoteDao extends CrudRepository<Note, Long> {
}
