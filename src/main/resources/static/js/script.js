let landingHeight = $('.landing').height();
let lastScrollPosition = 0;

$(window).scroll(function() {
  let wScroll = $(this).scrollTop();

  console.log(wScroll);

  if (wScroll > lastScrollPosition) {
    console.log('scroll down');
    $('header').css({'top': '-60px'});
  } else {
    console.log('scroll up');
    $('header').css({'top': 0});
  }

  lastScrollPosition = wScroll;

  if (wScroll <= 5) {
    $('.logo').css({'height': '20em'});

    $('.landing h3').css({
      'opacity': 1
    });

    $('.landing p').css({
      'opacity': 1
    });
  } else if (wScroll <= landingHeight) {
    $('.logo').css({'height': '30em'});

    $('.landing h3').css({
      'opacity': 0
    });

    $('.landing p').css({
      'opacity': 0
    });
  }
});

$(document).ready(function() {
  $('#newNote').click(function() {
    $('#modal').css({
      'visibility': 'visible',
      'opacity': 1
    });
  });

  $('.modal-close').click(function() {
    $('#modal').css({
      'visibility': 'hidden',
      'opacity': 0
    });
  });
});
